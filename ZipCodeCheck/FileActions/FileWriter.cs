using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace ZipCodeCheck{
    public class FileWriter{
        public void Writer(string zipCodeInfo){
            string? activeUser = Environment.GetEnvironmentVariable("USER"); 
            string FilePath = $"/home/{activeUser}/ceps";
            
            File.WriteAllText(FilePath, zipCodeInfo);
        }
    }
}