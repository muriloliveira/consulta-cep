using System;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;
 
namespace ZipCodeCheck{
    public class ZipCode{
        public static async Task ZipCodeApiAsync(List<string> ZipCodeList)
        {
            string dt = "";
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    foreach (string zipCode in ZipCodeList)
                    {
                        if(!string.IsNullOrEmpty(zipCode) && !Regex.IsMatch(zipCode, "[a-zA-Z]")){
                            string apiUrl = $"https://viacep.com.br/ws/{zipCode}/json/";
                            HttpResponseMessage response = await client.GetAsync(apiUrl);

                            if (response.IsSuccessStatusCode)
                            {
                                var jsonContent = await response.Content.ReadAsStringAsync();
                                var data = JsonSerializer.Deserialize<Address>(jsonContent);

                                if(data is not null){  
                                   dt += $"\nCEP: {data.cep}, Logradouro: {data.logradouro}, Cidade: {data.localidade}, UF: {data.uf}";
                                }

                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("O CEP não foi identificado na base.");
                                }
                            }
                        }      
                    }
                    Console.WriteLine("Terminou!");

                    FileWriter fileWriter = new FileWriter();
                    fileWriter.Writer(dt);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Erro na requisição: {ex}");
                }
            }
        }

    }
}
 